<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\TaskController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::middleware('auth')->group(function(){
    Route::get('/dashboard', function () {
        return view('admin.pages.dashboard');
    })->name('dashboard');
    Route::get('/product',[ProductController::class,'all'])->name('product.index');
    Route::get('/product/create',[ProductController::class,'create'])->name('product.create');
    Route::get('/task',[TaskController::class,'all'])->name('task.index');
    Route::get('/task/create',[TaskController::class,'create'])->name('task.create');
    Route::get('/logout',[LoginController::class,'logout'])->name('logout');

});
Route::get('login',function(){
    return view('admin.login');
})->name('auth.login');
Route::post('login',[LoginController::class,'login'])->name('login');