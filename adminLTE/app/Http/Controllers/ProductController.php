<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
class ProductController extends Controller
{
    public function all(){
        $products = Product::all();
        return view('admin.pages.product.products')->with('products',$products);
    }
    public function create(){
        $this->authorize('create',Product::class);
        return "cc";
    }
}
