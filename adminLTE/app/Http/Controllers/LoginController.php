<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
    public function login(Request $request){
        $validate = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ],[
            'email.required' => 'Email không được trống',
            'email.email' => 'Email chưa đúng cú pháp',
            'password.required' => 'Mật khẩu không được trống'
        ]
        );
        if(Auth::attempt($validate)){
               session()->flash('success');
               return redirect()->intended('dashboard');
        }
        else{
            return back()->withErrors(['failed' => 'Tài khoản or mật khẩu không đúng'])->withInput();
        }
    }
    public function logout(){
        Auth::logout();
        session()->flush();
        return redirect()->route('auth.login');
    }
}
