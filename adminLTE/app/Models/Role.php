<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $table = 'role';
    public const IS_CUSTOMER = 1;
    public const IS_ADMIN = 2;
    public const IS_MANAGER = 3;

}
