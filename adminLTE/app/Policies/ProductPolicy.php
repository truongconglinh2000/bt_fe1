<?php

namespace App\Policies;

use App\Models\Product;
use App\Models\User;
use App\Models\Role;

use Illuminate\Auth\Access\HandlesAuthorization;
use Auth;
class ProductPolicy
{
    use HandlesAuthorization;
    public function view(User $user, Product $product)
    {
        //
    }

    public function create(User $user)
    {
        return $user->role_id == Role::IS_ADMIN;
    }

    public function update(User $user, Product $product)
    {
        return $user->id == $product->user_id || in_array($user->role_id, [Role::IS_ADMIN,Role::IS_MANAGER]);
    }

    public function delete(User $user, Product $product)
    {
        return $user->id == $product->user_id || in_array($user->role_id, [Role::IS_ADMIN,Role::IS_MANAGER]);


    }

    public function forceDelete(User $user, Product $product)
    {
        //
    }
}
