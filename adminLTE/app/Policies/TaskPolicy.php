<?php

namespace App\Policies;

use App\Models\Task;
use App\Models\User;
use App\Models\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Task $task)
    {
        //
    }

    public function create(User $user)
    {
        return $user->role_id == Role::IS_ADMIN;
    }

    public function update(User $user, Task $task)
    {
        return $user->id == $task->user_id || in_array($user->role_id, [Role::IS_ADMIN,Role::IS_MANAGER]);
      
    }
 
    public function delete(User $user, Task $task)
    {
        return $user->id == $task->user_id || in_array($user->role_id, [Role::IS_ADMIN,Role::IS_MANAGER]);
  
    }
   
    public function forceDelete(User $user, Task $task)
    {
        //
    }
}
