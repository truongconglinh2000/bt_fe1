<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //\App\Models\User::factory(1)->create();
        \App\Models\Task::factory(10)->create();
        \App\Models\Product::factory(10)->create();
        \App\Models\User::create([
            'name' => 'conglinh',
            'email' => 'clinh@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('123456'),
            'remember_token' => Str::random(10),
            'role_id' => 1
        ]);
        \App\Models\User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('123456'),
            'remember_token' => Str::random(10),
            'role_id' => 2

        ]);
        \App\Models\User::create([
            'name' => 'manager',
            'email' => 'mana@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('123456'),
            'remember_token' => Str::random(10),
            'role_id' => 3

        ]);
        \App\Models\Role::create([
            'name' => 'Customer',   
        ]);
        \App\Models\Role::create([
            'name' => 'Admin',   
        ]);
        \App\Models\Role::create([
            'name' => 'Manager',   
        ]);

    }
}
