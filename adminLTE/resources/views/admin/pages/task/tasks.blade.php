@extends('admin.main')
@section('content')
<div class="content-wrapper">
   <h3>Task</h3>
    @can('create',\App\Models\Task::class)
    <a href="{{route('task.create')}}" class="btn btn-primary" style="margin: 0 0 10px 50px">Create New</a>
    @endcan
   <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name Task</th>
      <th scope="col">Description</th>
      <th scope="col">Status</th>
      <th scope="col">Edit</th>
    </tr>
  </thead>
  <tbody>
      @foreach($tasks as $key => $task)
    <tr>
      <th scope="row">{{$key++}}</th>
      <td>{{$task->name}}</td>
      <td>{{$task->description}}</td>
      <td>
          @if($task->status == 1)
          <i class="fas fa-check" style="color:green"></i>
          @elseif($task->status == 0)
          <i class="fas fa-times" style="color:red"></i>
          @endif
      </td>
      <td>
          @can('update',$task)
          <button class="btn btn-primary">Update</button>
          @endcan
          @can('delete',$task)
          <button class="btn btn-danger">Delete</button>
          @endcan
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endsection
<style>
    .content-wrapper table{
        width: 90%;
        margin: auto;
        background: white;
    }
    .content-wrapper h3{
        padding: 15px 0 0 50px;
    }
</style>
