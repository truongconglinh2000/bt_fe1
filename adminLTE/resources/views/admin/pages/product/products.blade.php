@extends('admin.main')
@section('content')
<div class="content-wrapper">
   <h3>Product</h3>
    @can('create',\App\Models\Product::class)
    <a href="{{route('product.create')}}" class="btn btn-primary" style="margin: 0 0 10px 50px">Create New</a>
    @endcan
   <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name Product</th>
      <th scope="col">Price</th>
      <th scope="col">Edit</th>
    </tr>
  </thead>
  <tbody>
      @foreach($products as $key => $product)
    <tr>
      <th scope="row">{{$key++}}</th>
      <td>{{$product->name}}</td>
      <td>{{number_format($product->price,0,".",",")}}</td>
      <td>
          @can('update',$product)
          <button class="btn btn-primary">Update</button>
          @endcan
          @can('delete',$product)
          <button class="btn btn-danger">Delete</button>
          @endcan
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endsection
<style>
    .content-wrapper table{
        width: 90%;
        margin: auto;
        background: white;
    }
    .content-wrapper h3{
        padding: 15px 0 0 50px;
    }
</style>
